package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

// Nasabah
type Nasabah struct{
	Norekening 	string `json:"Norekening"`
	Nama 		string `json:"Nama"`
	Status 		string `json:"Status"`
	Tgl_lahir 	string `json:"Tgl_lahir"`
	Alamat 		string `json:"Alamat"`
	Kode_Cabang string `json:"Kode_Cabang"`
	Jumlah_Tabaungan int `json:"Jumlah_Tabaungan"`
}

// Riwayat Tabungan, nantinya akan mencatat transaksi dari nasabah
type RiwayatTabungan struct{
	Id 				string `json:"Id"`
	Norekening 		string `json:"Norekening"`
	Tgl 			string `json:"Tgl"`
	Status 			string `json:"Status"`
	Jumlah 			int `json:"Jumlah"`
	Jumlah_Akhir 	int `json:"Jumlah_Akhir"`
}
// Data Awal
var Nasabahs =[]Nasabah{
	Nasabah{Norekening:"05523221", Status: "Aktif", Nama: "Nama1", Tgl_lahir: "2000-10-10", Alamat: "Manado", Kode_Cabang: "Mantos", Jumlah_Tabaungan: 100000},
	Nasabah{Norekening:"05523222", Status: "Aktif", Nama: "Nama2", Tgl_lahir: "2000-10-23", Alamat: "Manado", Kode_Cabang: "Malalayang", Jumlah_Tabaungan: 120000},
}
var RiwayatTabungans = []RiwayatTabungan{
	RiwayatTabungan{Id:"1", Norekening:"05523221", Status: "Debit", Tgl: "2020-10-10", Jumlah: 20000, Jumlah_Akhir: 100000},
	RiwayatTabungan{Id:"1", Norekening:"05523222", Status: "Kredit", Tgl: "2020-10-23", Jumlah: 20000, Jumlah_Akhir: 100000},
}


func homePage(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Selamat Datang")
	fmt.Println("Endpoint Hit: homePage")
}

// Bagian nasaba //////////////////////////////////////////////////////////////////
// mengambil data semua nasabah
func returnAllNasabahs(w http.ResponseWriter, r *http.Request){
	fmt.Println("Endpoint Hint: return all Nasabah")
	json.NewEncoder(w).Encode(Nasabahs)
}

// mengambil data nasabah
func returnSingleNasabah(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	key := vars["Norekening"]
	for _, nasaba := range Nasabahs{
		if nasaba.Norekening == key {
			json.NewEncoder(w).Encode(nasaba)
		}
	}
	fmt.Println("Endpoint Hint: return Single Nasabah")
}
// membuat nasabah baru
func createNewNasabah(w http.ResponseWriter, r *http.Request){
	reqBody, _ := ioutil.ReadAll(r.Body)
	var nasaba Nasabah
	json.Unmarshal(reqBody, &nasaba)
	Nasabahs = append(Nasabahs, nasaba)
	json.NewEncoder(w).Encode(nasaba)
	fmt.Println("Endpoint Hint: create Nasabah")
}
// mengubah data nasabah
func updateNasabah(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	norekening := vars["Norekening"]
	
	reqBody, _ := ioutil.ReadAll(r.Body)
	var nas Nasabah
	json.Unmarshal(reqBody, &nas)

	for nasaba := range Nasabahs{
		if Nasabahs[nasaba].Norekening == norekening {
			if len(nas.Nama) > 0 { Nasabahs[nasaba].Nama=nas.Nama }
			if len(nas.Status) > 0 { Nasabahs[nasaba].Status=nas.Status }
			if len(nas.Tgl_lahir) > 0 { Nasabahs[nasaba].Tgl_lahir=nas.Tgl_lahir }
			if len(nas.Kode_Cabang) > 0 { Nasabahs[nasaba].Kode_Cabang=nas.Kode_Cabang }
			json.NewEncoder(w).Encode(Nasabahs[nasaba])
		}
	}
	fmt.Println("Endpoint Hint: Update Nasabah")
}
// menghapus data nasabah
func deleteNasabah(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	norekening := vars["Norekening"]

	for index, nasaba := range Nasabahs{
		if nasaba.Norekening == norekening {
			Nasabahs= append(Nasabahs[:index], Nasabahs[index+1:]...)
		}
	}
	fmt.Println("Endpoint Hint: delete Nasabah")
}
// End Bagian Nasabah///////////////////////////////////////////////////////
// Bagian RiwayatTabungan//////////////////////////////////////////////////
// mengambil data semua riwayat tabungan
func returnAllRiwayatTabungan(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	key := vars["Norekening"]

	for _, riwayatTabungan := range RiwayatTabungans{
		if riwayatTabungan.Norekening == key {
			json.NewEncoder(w).Encode(riwayatTabungan)
		}
	}
	fmt.Println("Endpoint Hint: return All Riwayat Tabungan Nasabah")
	
}
// membuat riwayat tabungan baru
func createNewRiwayatTabungan(w http.ResponseWriter, r *http.Request){
	reqBody, _ := ioutil.ReadAll(r.Body)
	var riwayatTabungan RiwayatTabungan
	json.Unmarshal(reqBody, &riwayatTabungan)

	if riwayatTabungan.Jumlah >= 0 {
		riwayatTabungan.Status="Debit"
	}else{
		riwayatTabungan.Status="Kredit"
	}

	fmt.Println(riwayatTabungan.Norekening)
	var jumlahSebelum = getJumlahTabunganSekarang(riwayatTabungan.Norekening)
	var jumlahSesudah =jumlahAkhirTabungan( jumlahSebelum, riwayatTabungan.Jumlah)
	setJumlahTabunganSekarang(riwayatTabungan.Norekening, jumlahSesudah)
	riwayatTabungan.Jumlah_Akhir=jumlahSesudah

	RiwayatTabungans = append(RiwayatTabungans, riwayatTabungan)
	json.NewEncoder(w).Encode(riwayatTabungan)
	fmt.Println("Endpoint Hint: create Riwayat Tabungan Baru / Transaksi")
}

// fungsi//////////////////////////////////////////////
// menghitung jumlah akhir tabungan
func jumlahAkhirTabungan(n int, l int)int{
	return n+l
}
// mengambil jumlah tabungan sekarang nasabah
func getJumlahTabunganSekarang(norekening string) int{
	for _, nasaba := range Nasabahs{
		if nasaba.Norekening == norekening {
			return nasaba.Jumlah_Tabaungan
		}
	}
	return 0;
}
// mengatur ulang jumlah tabungan nasabah
func setJumlahTabunganSekarang(norekening string, jumlah int){
	for nasaba := range Nasabahs{
		if Nasabahs[nasaba].Norekening == norekening {
			Nasabahs[nasaba].Jumlah_Tabaungan=jumlah
		}
	}
}
// End Bagian RiwayatTabungan/////////////////////////////////////////////////////////////////

func handleRequests(){
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	
	myRouter.HandleFunc("/nasabahs", returnAllNasabahs)
	myRouter.HandleFunc("/nasabah", createNewNasabah).Methods("POST")
	myRouter.HandleFunc("/nasabah/{Norekening}", updateNasabah).Methods("PUT")
	myRouter.HandleFunc("/nasabah/{Norekening}", deleteNasabah).Methods("DELETE")
	myRouter.HandleFunc("/nasabah/{Norekening}", returnSingleNasabah)
	
	myRouter.HandleFunc("/riwayat-tabungan/{Norekening}", returnAllRiwayatTabungan)
	myRouter.HandleFunc("/riwayat-tabungan", createNewRiwayatTabungan).Methods("POST")

	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func main(){
	handleRequests()
}